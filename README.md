About:
This app implements fundamental data structure design for creating various Binary Search Trees. I have used Goodrich and Tamassia's "Algorithm design" book for the algorithms I have implemented in this app.

Source files:
/Tree Traversal/Source/Model: this path includes all the source files for the trees
The trees that I have implemented:
Binary Search Tree(finished)
AVL tree(finished)
Red-Black Tree(delete function needs more cases)
Binary Min-Heap(finished)
Binary Max-Heap(finished)

Functionalitys as of now:
Creates trees based on UserInput and produces three fundamental walks: Pre-Order, In-Order, and Post-Order.

TODO:
come up with Algorithms to visualize the trees.


