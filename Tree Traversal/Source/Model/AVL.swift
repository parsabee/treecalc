//
//  AVL.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/18/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import Foundation
public class AVL{
	private var root: Node?;
	
	init() {
		root = nil;
	}
	func getRoot() -> Node?{
		return root;
	}
	private func setRoot(root: Node){
		self.root = root
	}
	/*****************************************************
	helper get and set height functions
	getHeight: retrieves the height of a node
	setHeight: sets the height of a node to the maximum of
	its left and right children plus one
	*****************************************************/
	private func getHeight(node: Node?) -> Int{
		if node == nil{
			return 0;
		}
		return node!.getHeight();
	}
	private func setHeight(node: Node?){
		guard node == nil else{
			let n: Int =  max(((node!.getLeftChild()?.getHeight()) ?? 0), ((node!.getRightChild()?.getHeight()) ?? 0));
			node!.setHeight(height: n + 1);
			return;
		}
	}
	/*****************************************************
	helper function returns the balance factor of subtrees
	*****************************************************/
	private func getBalance(node: Node?) -> Int{
		if node == nil{
			return 0;
		}
		return getHeight(node: node!.getLeftChild()) - getHeight(node: node!.getRightChild());
	}
	/*******************************************************
	perform a regular insert, update the height as we insert
	check to see if the tree has maintained AVL properties
	*******************************************************/
	func insert(data: Float32) -> Int{
		/*************************
		insert node into the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		var y: Node? = nil;
		var x: Node? = root;
		while (x != nil && x?.getData() != nil){
			y = x;
			if (data < (x!.getData())!){
				x = x!.getLeftChild();
			}else if(data > (x!.getData()!)){
				x = x!.getRightChild();
			}else{
				return 0;
			}
		}
		/********************************
		create a node with the input data
		********************************/
		let z: Node = Node(data: data);
		z.setParent(parent: y);
		/***************************************
		CASE I:
		tree doens't have any data (root is nil)
		***************************************/
		if(y == nil){
			root = z;
		}
			/*********************
			CASE II:
			insert in left subtree
			*********************/
		else if(data < (y!.getData())!){
			y!.setLeftChild(left_child: z);
		}
			/**********************
			CASE III:
			insert in right subtree
			**********************/
		else if(data > (y!.getData())!){
			y!.setRightChild(right_child: z);
		}
		rebalanceTree(node: z);
		return 1;
	}
	/****************************************************************************
	private search method; returns the node that corresponds with the input data.
	returns nil if data not found.
	****************************************************************************/
	private func search(data: Float32) -> Node?{
		var done: Bool = false;
		var temp: Node? = root;
		if temp == nil {
			return nil
		}
		while(!done){
			if(temp == nil){
				return nil;
			}
			if((temp?.getData())! == data){
				done = true;
			}
			else if((temp?.getData())! > data){
				temp = temp?.getLeftChild();
			}
			else if((temp?.getData())! < data){
				temp = temp?.getRightChild();
			}
		}
		return temp;
	}
	/************************************
	delete method and its healper methods
	************************************/
	func delete(data: Float32) -> Int{
		/*************************
		delete node from the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		if let to_be_deleted: Node = search(data: data){
			AVLDelete(z: to_be_deleted);
			return 1;
		}
		return 0;
	}
	/********************************
	deleting nodes from the tree,
	updating the heights alog the way
	********************************/
	private func AVLDelete(z: Node){
		var y: Node = z;
		var x: Node?
		/******************
		CASE I:
		z has no left child
		******************/
		if(z.getLeftChild() == nil){
			x = z.getParent();
			transplant(u: z, v: z.getRightChild());
			while (x != nil){/**updating the heights from bottom up**/
				setHeight(node: x)
				x = x?.getParent()
			}
			rebalanceTree(node: z.getRightChild());
		}
			/*******************
			CASE II:
			z has no Right child
			*******************/
		else if(z.getRightChild() == nil){
			x = z.getParent();
			transplant(u: z, v: z.getLeftChild());
			while (x != nil){/**updating the heights from bottom up**/
				setHeight(node: x)
				x = x?.getParent()
			}
			rebalanceTree(node: z.getLeftChild());
		}
			/*********************************************
			CASE III:
			z has both children
			get the max of left subtree, then fix the tree
			*********************************************/
		else{
			x = getMax(top: z.getLeftChild()!);
			y = x!;
			if (y.getParent()?.getData() != z.getData()){
				var top: Node? = y.getParent();/**record the parent of the node that will replace z to update the heights**/
				transplant(u: y, v: y.getLeftChild());
				while (top != nil){/**updating the heights from bottom up**/
					setHeight(node: top)
					top = top?.getParent()
				}
				y.setLeftChild(left_child: z.getLeftChild());
				if y.getLeftChild() != nil{
					y.getLeftChild()!.setParent(parent: y);
				}
			}
			transplant(u: z, v: y);
			y.setRightChild(right_child: z.getRightChild());
			y.getRightChild()!.setParent(parent: y);
			rebalanceTree(node: x?.getLeftChild());
		}
	}
	private func rebalanceTree(node: Node?){
		var y: Node? = node;
		/****************************
		itearatively balance the tree
		****************************/
		while (y != nil){
			y = y?.getParent()
			let balance: Int = getBalance(node: y);
			if (balance >= 2){
				if (getBalance(node: y?.getLeftChild()) >= 0){
					/********
					CASE I:
					left left
					********/
					rightRotate(node: y!);
				}
				else{
					/*********
					CASE II:
					left right
					*********/
					leftRotate(node: (y!.getLeftChild())!);
					setHeight(node: (y!.getLeftChild()!.getLeftChild())!)
					rightRotate(node: y!);
				}
			}
			else if(balance <= -2){
				if (getBalance(node: y?.getRightChild()) <= 0){
					/**********
					CASE III:
					right right
					**********/
					leftRotate(node: y!);
				}
				else{
					/*********
					CASE IV:
					right left
					*********/
					rightRotate(node: (y!.getRightChild())!);
					setHeight(node: (y!.getRightChild()!.getRightChild())!)
					leftRotate(node: y!);
				}
			}
			setHeight(node: y);
		}
	}
	private func getMax(top: Node) -> Node{
		/***********************************************************************
		takes the root of a subtree and returns the maximum of its right subtree
		***********************************************************************/
		var done: Bool = false;
		var temp:Node? = top;
		while(!done){
			if (temp?.getRightChild() == nil){
				done = true;
			}
			else{
				temp = temp?.getRightChild();
			}
		}
		return temp!;
	}
	private func transplant(u: Node?, v: Node?){
		if(u?.getParent() == nil){
			root = v;
		}
		else if(u?.getData() == u?.getParent()?.getLeftChild()?.getData()){
			u?.getParent()?.setLeftChild(left_child: v);
		}
		else{
			u?.getParent()?.setRightChild(right_child: v);
		}
		if(v != nil){
			v?.setParent(parent: u?.getParent())
		}
	}
	/****************
	Rotation Handling
	****************/
	private func leftRotate(node: Node){
		/******************************************************
		node: the pivot of rotation
		y: node's right child
		by the end of this function:
		node's parent = y, node's right child = y's left child;
		y's parent = node's parent, y's left child = node;
		******************************************************/
		let y: Node = node.getRightChild()!;
		if(y.getLeftChild() != nil){
			y.getLeftChild()!.setParent(parent: node);
		}
		node.setRightChild(right_child: y.getLeftChild());
		y.setLeftChild(left_child: node);
		y.setParent(parent: node.getParent());
		if(node.getParent() != nil){
			if(node.getParent()!.getLeftChild()?.getData() ?? Float32(Int.max) == node.getData()){
				node.getParent()!.setLeftChild(left_child: y);
			}
			else if(node.getParent()!.getRightChild()?.getData() ?? Float32(Int.max) == node.getData()){
				node.getParent()!.setRightChild(right_child: y);
			}
		}
		node.setParent(parent: y);
		if(self.getRoot()?.getData() == node.getData()){
			root = y
		}
	}
	private func rightRotate(node: Node){
		/******************************************************
		node: the pivot of rotation
		x: node's left child
		by the end of this function:
		node's parent = x, node's left child = x's right child;
		x's parent = node's parent, x's right child = node;
		******************************************************/
		let x: Node = node.getLeftChild()!;
		if(x.getRightChild() != nil){
			x.getRightChild()!.setParent(parent: node);
		}
		node.setLeftChild(left_child: x.getRightChild());
		x.setRightChild(right_child: node);
		x.setParent(parent: node.getParent())
		if(node.getParent() != nil){
			if(node.getParent()!.getRightChild()?.getData() ?? Float32(Int.max) == node.getData()!){
				node.getParent()!.setRightChild(right_child: x)
			}
			else if(node.getParent()!.getLeftChild()?.getData() ?? Float32(Int.max) == node.getData()){
				node.getParent()!.setLeftChild(left_child: x)
			}
		}
		node.setParent(parent: x);
		if(self.getRoot()!.getData() == node.getData()){
			root = x;
		}
	}
	deinit {
		print("AVL KILLED")
	}
}
