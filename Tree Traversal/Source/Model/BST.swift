//
//  BST.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/21/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

class BST{
	private var root: Node?;
	init(){
		root = nil
	}
	func getRoot() -> Node?{
		return root;
	}
	/************
	Insert Method
	************/
	func insert(data: Float32) -> Int{
		/*************************
		insert node into the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		var y: Node? = nil;
		var x: Node? = root;
		while (x != nil && x?.getData() != nil){
			y = x;
			if (data < (x!.getData())!){
				x = x!.getLeftChild();
			}else if (data > (x!.getData())!){
				x = x!.getRightChild();
			}else{
				return 0
			}
		}
		/********************************
		node doesn't exist already,
		create a node with the input data
		********************************/
		let z: Node = Node(data: data);
		z.setParent(parent: y);
		/***************************************
		CASE I:
		tree doens't have any data (root is nil)
		***************************************/
		if(y == nil || y?.getData() == nil){
			root = z;
		}
			/*********************
			CASE II:
			insert in left subtree
			*********************/
		else if(data < (y!.getData())!){
			y!.setLeftChild(left_child: z);
		}
			/**********************
			CASE III:
			insert in right subtree
			**********************/
		else if(data > (y!.getData())!){
			y!.setRightChild(right_child: z);
		}
		z.setColor(color: "R");
		return 1
	}
	/****************************************************************************
	private search method; returns the node that corresponds with the input data.
	returns nil if data not found.
	****************************************************************************/
	private func search(data: Float32) -> Node?{
		var done: Bool = false;
		var temp: Node? = root;
		if temp == nil {
			return nil
		}
		while(!done){
			if(temp == nil){
				return nil;
			}
			if((temp?.getData())! == data){
				done = true;
			}
			else if((temp?.getData())! > data){
				temp = temp?.getLeftChild();
			}
			else if((temp?.getData())! < data){
				temp = temp?.getRightChild();
			}
		}
		return temp;
	}
	/************************************
	delete method and its healper methods
	************************************/
	func delete(data: Float32) -> Int{
		/*************************
		delete node from the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		if let to_be_deleted: Node = search(data: data){
			BSTDelete(z: to_be_deleted);
			return 1;
		}
		return 0;
	}
	private func BSTDelete(z: Node){
		var y: Node = z;
		var x: Node?
		/******************
		CASE I:
		z has no left child
		******************/
		if(z.getLeftChild() == nil){
			x = z.getRightChild();
			transplant(u: z, v: z.getRightChild());
		}
			/*******************
			CASE II:
			z has no Right child
			*******************/
		else if(z.getRightChild() == nil){
			x = z.getLeftChild();
			transplant(u: z, v: z.getLeftChild());
		}
			/************************************************
			CASE III:
			z has both children
			get the max of left subtree, then fix the RB tree
			************************************************/
		else{
			y = getMax(top: z.getLeftChild()!);
			x = y.getLeftChild();
			if(y.getParent()!.getData() == z.getData()){
				if x != nil{
					x!.setParent(parent: y);
				}
			}
			else{
				transplant(u: y, v: y.getLeftChild());
				y.setLeftChild(left_child: z.getLeftChild());
				y.getLeftChild()!.setParent(parent: y);
			}
			transplant(u: z, v: y);
			y.setRightChild(right_child: z.getRightChild());
			y.getRightChild()!.setParent(parent: y);
			y.setColor(color: z.getColor()!);
		}
	}
	private func getMax(top: Node) -> Node{
		/***********************************************************************
		takes the root of a subtree and returns the maximum of its right subtree
		***********************************************************************/
		var done: Bool = false;
		var temp:Node? = top;
		while(!done){
			if (temp?.getRightChild() == nil){
				done = true;
			}
			else{
				temp = temp?.getRightChild();
			}
		}
		return temp!;
	}
	private func transplant(u: Node?, v: Node?){
		if(u?.getParent() == nil){
			root = v;
		}
		else if(u?.getData() == u?.getParent()?.getLeftChild()?.getData()){
			u?.getParent()?.setLeftChild(left_child: v);
		}
		else{
			u?.getParent()?.setRightChild(right_child: v);
		}
		if(v != nil){
			v?.setParent(parent: u?.getParent())
		}
	}
	deinit {
		print("BST KILLED")
	}
}
