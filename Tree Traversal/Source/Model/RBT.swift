//
//  RBT.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/11/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import Foundation
public class RBT{
	private var root: Node?;
	init() {
		root = nil;
	}
	func getRoot() -> Node?{
		return root;
	}
	private func setRoot(root: Node){
		self.root = root
	}
	/**********************************************************************
	the following two functions get the maximum and the minimum of the tree
	**********************************************************************/
	private func getMax(top: Node) -> Node{
//		var done: Bool = false;
//		var temp:Node? = top;
//		while(!done){
//			if (temp?.getRightChild() == nil){
//				done = true;
//			}
//			else{
//				temp = temp?.getRightChild();
//			}
//		}
//		return temp!;
		var x: Node = top;
		while(x.getRightChild() != nil){
			x = x.getRightChild()!;
		}
		return x;
	}
	private func getMin(top: Node) -> Node{
		var x: Node = top;
		while(x.getLeftChild() != nil){
			x = x.getLeftChild()!;
		}
		return x;
	}
	/********************************************************
	the following function determines the successor of a node
	based on the trees in-order walk
	********************************************************/
	private func successor(node: Node) -> Node?{
		var x:Node = node;
		if (x.getRightChild() != nil){
			return getMin(top: x.getRightChild()!);
		}
		var y:Node? = x.getParent();
		while (y != nil && x.getData() == y!.getRightChild()!.getData()!){
			x = y!;
			y = y!.getParent();
		}
		return y
	}
	/*****************************************************
	perform a regular insert,
	check to see if the tree has maintained RBT properties
	*****************************************************/
	func insert(data: Float32) -> Int{
		/*************************
		insert node into the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		var y: Node? = nil;
		var x: Node? = root;
		while (x != nil && x?.getData() != nil){
			y = x;
			if (data < (x!.getData())!){
				x = x!.getLeftChild();
			}else if(data > (x!.getData()!)){
				x = x!.getRightChild();
			}else{
				return 0;
			}
		}
		/********************************
		create a node with the input data
		********************************/
		let z: Node = Node(data: data);
		z.setParent(parent: y);
		/***************************************
		CASE I:
		tree doens't have any data (root is nil)
		***************************************/
		if(y == nil || y?.getData() == nil){
			root = z;
		}
		/*********************
		CASE II:
		insert in left subtree
		*********************/
		else if(data < (y!.getData())!){
			y!.setLeftChild(left_child: z);
		}
		/**********************
		CASE III:
		insert in right subtree
		**********************/
		else if(data > (y!.getData())!){
			y!.setRightChild(right_child: z);
		}
		z.setColor(color: "R");
		fixInsert(x: z);
		return 1;
	}
	/***********************************************************************************
	private method used after insert to fix the tree if it doens't comply RBT properties
	***********************************************************************************/
	private func fixInsert(x: Node){
		var z: Node? = x;
		var y: Node? = nil;
		while((z?.getParent()?.getColor() == "R")){
			if(z?.getParent()?.getData() == z?.getParent()?.getParent()?.getLeftChild()?.getData()){
				y = z?.getParent()?.getParent()?.getRightChild();
				if(y?.getColor() == "R"){
					/*****
					CASE I
					*****/
					z?.getParent()?.setColor(color: "B");
					y?.setColor(color: "B");
					z?.getParent()?.getParent()?.setColor(color: "R");
					z = z?.getParent()?.getParent();
				}
				else {
					if(z?.getData() == z?.getParent()?.getRightChild()?.getData()){
						/******
						CASE II
						******/
						z = z?.getParent();
						if z != nil{
							leftRotate(node: z!);
						}
					}
					/*******
					CASE III
					*******/
					if z!.getParent()!.getParent() != nil {
						z!.getParent()!.setColor(color: "B");
						z!.getParent()!.getParent()!.setColor(color: "R");
						rightRotate(node: (z!.getParent()!.getParent())!);
					}
				}
			}
			else if(z?.getParent()?.getData() == z?.getParent()?.getParent()?.getRightChild()?.getData()){
				y = z?.getParent()?.getParent()?.getLeftChild();
				if(y?.getColor() == "R"){
					/*****
					CASE I
					*****/
					z?.getParent()?.setColor(color: "B");
					y?.setColor(color: "B");
					z?.getParent()?.getParent()?.setColor(color: "R");
					z = z?.getParent()?.getParent();
				}
				else{
					if(z?.getData() == z?.getParent()?.getLeftChild()?.getData()){
						/******
						CASE II
						******/
						z = z?.getParent();
						if z != nil {
							rightRotate(node: z!)
						}
					}
					/*******
					CASE III
					*******/
					if z!.getParent()!.getParent() != nil{
						z!.getParent()!.setColor(color: "B");
						z!.getParent()!.getParent()!.setColor(color: "R");
						leftRotate(node: (z!.getParent()!.getParent())!);
					}
				}
			}
		}
		root!.setColor(color: "B");
	}
	/****************************************************************************
	private search method; returns the node that corresponds with the input data.
	returns nil if data not found.
	****************************************************************************/
	private func search(data: Float32) -> Node?{
		var done: Bool = false;
		var temp: Node? = root;
		if temp == nil {
			return nil
		}
		while(!done){
			if(temp == nil){
				return nil;
			}
			if((temp?.getData())! == data){
				done = true;
			}
			else if((temp?.getData())! > data){
				temp = temp?.getLeftChild();
			}
			else if((temp?.getData())! < data){
				temp = temp?.getRightChild();
			}
		}
		return temp;
	}
	/************************************
	delete method and its healper methods
	************************************/
	func delete(data: Float32) -> Int{
		/*************************
		delete node from the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		if let to_be_deleted: Node = search(data: data){
			RBDelete(z: to_be_deleted);
			return 1;
		}
		return 0;
	}
	private func RBDelete(z: Node){
		var replaceNode: Node? = nil; /*tracking the node that will replace*/
		if(z.getData() != nil){
			var removedOrMovedNode: Node = z;
			var removedOrMovedNodeColor: Character = removedOrMovedNode.getColor()!;
			
			if(z.getLeftChild()?.getData() == nil || z.getLeftChild() == nil){
				replaceNode = z.getRightChild();
				RBTransplant(u: z, v: z.getRightChild());
			}else if(z.getRightChild()?.getData() == nil || z.getRightChild() == nil){
				replaceNode = z.getLeftChild();
				RBTransplant(u: z, v: z.getLeftChild());
			}else{
				removedOrMovedNode = getMin(top: z.getRightChild()!);
				removedOrMovedNodeColor = removedOrMovedNode.getColor()!;
				replaceNode = removedOrMovedNode.getRightChild();
				if(removedOrMovedNode.getParent()?.getData() == z.getData()){
					replaceNode?.setParent(parent: removedOrMovedNode);
				}else{
					RBTransplant(u: z, v: removedOrMovedNode);
					removedOrMovedNode.setRightChild(right_child: z.getRightChild());
					removedOrMovedNode.getRightChild()?.setParent(parent: removedOrMovedNode);
				}
				RBTransplant(u: z, v: removedOrMovedNode);
				removedOrMovedNode.setLeftChild(left_child: z.getLeftChild());
				removedOrMovedNode.getLeftChild()?.setParent(parent: removedOrMovedNode);
				removedOrMovedNode.setColor(color: z.getColor()!);
			}
			
			if(removedOrMovedNodeColor == "B"){
				fixDelete(node: replaceNode);
			}
		}

//		var y: Node? = z;
//		var x: Node?
//		var y_original_color: Character = y!.getColor()!;
//		/******************
//		CASE I:
//		z has no left child
//		******************/
//		if(z.getLeftChild() == nil || z.getLeftChild()?.getData() == nil){
//			x = z.getRightChild();
//			if(x == nil){/**create a helper node with nil data**/
//				x = Node(data: nil);
//				x!.setParent(parent: z);
//				x!.getParent()!.setRightChild(right_child: x);
//			}
//			RBTransplant(u: z, v: x);
//		}
//		/*******************
//		CASE II:
//		z has no Right child
//		*******************/
//		else if(z.getRightChild() == nil || z.getRightChild()?.getData() == nil){
//			x = z.getLeftChild();
//			if(x == nil){/**create a helper node with nil data**/
//				x = Node(data: nil);
//				x!.setParent(parent: z);
//				x!.getParent()!.setLeftChild(left_child: x);
//			}
//			RBTransplant(u: z, v: x);
//		}
//		/************************************************
//		CASE III:
//		z has both children
//		get the max of left subtree, then fix the RB tree
//		************************************************/
//		else{
//			y = getMax(top: z.getLeftChild()!);
//			y_original_color = (y?.getColor()!)!;
//			x = y?.getLeftChild();
//			if x == nil{/**create a helper node with nil data**/
//				x = Node(data: nil)
//				x!.setParent(parent: y)
//				y?.setLeftChild(left_child: x);
//			}
//			if(y?.getParent()!.getData() == z.getData()){
//				x?.setParent(parent: y);
//			}else{
//				RBTransplant(u: y, v: y?.getLeftChild());
//				y?.setLeftChild(left_child: z.getLeftChild());
//				y?.getLeftChild()!.setParent(parent: y);
//			}
//			RBTransplant(u: z, v: y);
//			y?.setRightChild(right_child: z.getRightChild())
//			y?.getRightChild()?.setParent(parent: y)
//			y?.setColor(color: z.getColor()!)
//		}
//		if(y_original_color == "B"){
//			fixDelete(node: x);
//		}else if x?.getData() == nil{
//			handleNil(x: &x)
//		}
	}
	private func RBTransplant(u: Node?, v: Node?){
		if(u?.getParent() == nil){
			root = v;
		}
		else if(u?.getData() == u?.getParent()?.getLeftChild()?.getData()){
			u?.getParent()?.setLeftChild(left_child: v);
		}
		else{
			u?.getParent()?.setRightChild(right_child: v);
		}
		if(v != nil){
			v?.setParent(parent: u?.getParent())
		}
	}
	private func fixDelete(node: Node?){
		var x: Node? = node;
		while (node?.getData() != root?.getData() && node?.getColor() == "B"){
			if(node?.getData() == node?.getParent()?.getLeftChild()?.getData()){
				var w: Node? = node?.getParent()?.getRightChild();
				if (w?.getColor() == "R"){
					w?.setColor(color: "B");
					node?.getParent()?.setColor(color: "R");
					leftRotate(node: (node?.getParent())!);
					w = node?.getParent()?.getRightChild();
				}
				if(w?.getLeftChild()?.getColor() == "B" && w?.getRightChild()?.getColor() == "B"){
					w?.setColor(color: "R");
					x = node?.getParent();
				}else if(w != nil){
					if(w?.getRightChild()?.getColor() == "B"){
						w?.getLeftChild()?.setColor(color: "B");
						w?.setColor(color: "R");
						rightRotate(node: w!);
						w = x?.getParent()?.getRightChild();
					}
					
				}
			}
		}
//		var nilNode: Node?
//		if node?.getData() == nil{
//			nilNode = node
//		}
//		var x: Node? = node
//		while(x?.getData() != root?.getData() && x?.getColor() == "B"){
//			if(x?.getData() == x?.getParent()?.getLeftChild()?.getData()){
//				var w: Node? = (x?.getParent()?.getRightChild())!;
//				if(w?.getColor() == "R"){
//					/*****
//					CASE I
//					*****/
//					w?.setColor(color: "B");
//					x?.getParent()?.setColor(color: "R");
//					leftRotate(node: (x?.getParent())!);
//					w = (x?.getParent()?.getRightChild())!;
//				}
//				if((w?.getLeftChild()?.getColor() == "B" || w?.getLeftChild() == nil) && (w?.getRightChild()?.getColor() == "B" || w?.getRightChild() == nil)){
//					/******
//					CASE II
//					******/
//					w?.setColor(color: "R");
//					x = x?.getParent();
//				}
//				else{
//					if(w?.getRightChild()?.getColor() == "B" || w?.getRightChild() == nil){
//						/*******
//						CASE III
//						*******/
//						w?.getLeftChild()?.setColor(color: "B");
//						w?.setColor(color: "R");
//						rightRotate(node: w!);
//						w = (x?.getParent()?.getRightChild())!;
//					}
//					/******
//					CASE IV
//					******/
//					w?.setColor(color: (x?.getParent()?.getColor())!);
//					x?.getParent()?.setColor(color: "B");
//					w?.getRightChild()?.setColor(color: "B");
//					leftRotate(node: (x?.getParent())!);
//					x = root;
//				}
//			}
//			else{
//				var w: Node? = (x?.getParent()?.getLeftChild())!;
//				if(w?.getColor() == "R"){
//					/*****
//					CASE I
//					*****/
//					w?.setColor(color: "B");
//					x?.getParent()?.setColor(color: "R");
//					rightRotate(node: (x?.getParent())!);
//					w = (x?.getParent()?.getLeftChild())!;
//				}
//				if(w?.getRightChild()?.getColor() == "B" && w?.getLeftChild()?.getColor() == "B"){
//					/******
//					CASE II
//					******/
//					w?.setColor(color: "R");
//					x = x?.getParent();
//				}
//				else{
//					if(w?.getLeftChild()?.getColor() == "B"){
//						/*******
//						CASE III
//						*******/
//						w?.getRightChild()?.setColor(color: "B");
//						w?.setColor(color: "R");
//						leftRotate(node: w!);
//						w = (x?.getParent()?.getLeftChild())!;
//					}
//					/******
//					CASE IV
//					******/
//					w?.setColor(color: (x?.getParent()?.getColor())!);
//					x?.getParent()?.setColor(color: "B");
//					w?.getLeftChild()?.setColor(color: "B");
//					rightRotate(node: (x?.getParent())!);
//					x = root
//				}
//			}
//		}
//		if nilNode != nil{
//			handleNil(x: &nilNode);/**delete the helper node with nil data**/
//		}
//		x?.setColor(color: "B");
	}
	/****************************************
	function to delete the node with nil data
	****************************************/
	private func handleNil(x: inout Node?){
		if root?.getData() == nil{
			root = nil
		}
		if x?.getParent()?.getLeftChild() != nil{
			x?.getParent()?.setLeftChild(left_child: nil);
		}else if x?.getParent()?.getRightChild() != nil{
			x?.getParent()?.setRightChild(right_child: nil);
		}
		x?.setParent(parent: nil);
		x?.setLeftChild(left_child: nil);
		x?.setRightChild(right_child: nil);
		x = nil;
	}
	/****************
	Rotation Handling
	****************/
	private func leftRotate(node: Node){
		let y: Node = node.getRightChild()!;
		if(y.getLeftChild() != nil){
			y.getLeftChild()!.setParent(parent: node);
		}
		node.setRightChild(right_child: y.getLeftChild());
		y.setLeftChild(left_child: node);
		y.setParent(parent: node.getParent());
		if(node.getParent() != nil){
			if(node.getParent()!.getLeftChild()?.getData() == node.getData()){
				node.getParent()!.setLeftChild(left_child: y);
			}
			else if(node.getParent()!.getRightChild()?.getData() == node.getData()){
				node.getParent()!.setRightChild(right_child: y);
			}
		}
		node.setParent(parent: y);
		if(self.getRoot()?.getData() == node.getData()){
			root = y
		}
	}
	private func rightRotate(node: Node){
		let x: Node = node.getLeftChild()!;
		if(x.getRightChild() != nil){
			x.getRightChild()!.setParent(parent: node);
		}
		node.setLeftChild(left_child: x.getRightChild());
		x.setRightChild(right_child: node);
		x.setParent(parent: node.getParent())
		if(node.getParent() != nil){
			if(node.getParent()!.getRightChild()!.getData() == node.getData()!){
				node.getParent()!.setRightChild(right_child: x)
			}
			else if(node.getParent()!.getLeftChild()!.getData() == node.getData()){
				node.getParent()!.setLeftChild(left_child: x)
			}
		}
		node.setParent(parent: x);
		if(self.getRoot()!.getData() == node.getData()){
			root = x;
		}
	}
	deinit {
		print("RBT KILLED")
	}
}
