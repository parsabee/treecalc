//
//  Tree.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/10/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import Foundation
import UIKit

class Tree{
	private var treeBST: BST? = nil;
	private var treeRBT: RBT? = nil;
	private var treeAVL: AVL? = nil;
	private var treeMax: MaxHeap? = nil;
	private var root: Node? = nil;
	var tree_type: String?;
	init(tree: String){
		tree_type = tree;
		switch tree{
		case "Binary Search Tree":
			treeBST = BST();
			break;
		case "Red-Black Tree":
			treeRBT = RBT();
			break;
		case "AVL Tree":
			treeAVL = AVL();
			break;
		default:
			break;
		}
	}
	/***********
	tree methods
	***********/
	func getTree()->Any?{
		switch tree_type!{
		case "Binary Search Tree":
			return treeBST;
		case "Red-Black Tree":
			return treeRBT;
		case "AVL Tree":
			return treeAVL;
		default:
			return nil;
		}
	}
	func getRoot() -> Node?{
		switch tree_type! {
		case "Binary Search Tree":
			return treeBST!.getRoot();
		case "Red-Black Tree":
			return treeRBT!.getRoot();
		case "AVL Tree":
			return treeAVL!.getRoot();
		default:
			break;
		}
		return nil;
	}
	func delete(data: Float32?) -> String?{
		switch tree_type! {
		case "Binary Search Tree":
			let x = treeBST!.delete(data: data!);
			if (x == 0){
				return "Deletion Failed: Node Doesn't Exist\n"
			}
			break;
		case "Red-Black Tree":
			let x = treeRBT!.delete(data: data!);
			if (x == 0){
				return "Deletion Failed: Node Doesn't Exist\n"
			}
			break;
		case "AVL Tree":
			let x = treeAVL!.delete(data: data!);
			if (x == 0){
				return "Deletion Failed: Node Doesn't Exist\n"
			}
			break;
		default:
			break
		}
		return nil;
	}
	func insert(data: Float32?) -> String?{
		switch tree_type!{
		case "Binary Search Tree":
			let x = treeBST!.insert(data: data!);
			if (x == 0){
				return "Insertion Failed: Node Already Exists\n"
			}
			break;
		case "Red-Black Tree":
			let x = treeRBT!.insert(data: data!);
			if (x == 0){
				return "Insertion Failed: Node Already Exists\n"
			}
			break;
		case "AVL Tree":
			let x = treeAVL!.insert(data: data!);
			if (x == 0){
				return "Insertion Failed: Node Already Exists\n"
			}
			break;
		default:
			break;
		}
		return nil
	}
	/**************************
	Iterative traversal methods
	**************************/
	private func inOrder(top t: Node?) -> NSMutableAttributedString{
		/******************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) left child, 2) root, 3) right child
		******************************************************/
		let traversal: String = "IN-ORDER\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.gray])
		if tree_type! == "Red-Black Tree"{
			/***********************************************************
			if tree type is red black, this will color the red nodes red
			***********************************************************/
			if t?.getData() != nil{
				var stack: [Node?] = []
				var node: Node! = t
				while node != nil {
					stack.append(node)
					node = node!.getLeftChild()
				}
				while stack.count > 0{
					node = stack.popLast()!
					if node.getData() != nil{
						if node.getColor() == "R"{
							let nodeData = NSMutableAttributedString(string: String(describing: (node.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.red]);
							attributedTraversal.append(nodeData);
						}else{
						let nodeData = NSMutableAttributedString(string: String(describing: (node.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
							attributedTraversal.append(nodeData);
						}
					}
					if node.getRightChild() != nil{
						node = node.getRightChild()
						while(node != nil){
							stack.append(node)
							node = node.getLeftChild()
						}
					}
				}
			}
		}
		else if(tree_type! == "AVL Tree"){
			/**********************************************************
			if tree type is AVL, it will print the height of every node
			**********************************************************/
			if t?.getData() != nil{
				var stack: [Node?] = []
				var node: Node! = t
				while node != nil {
					stack.append(node)
					node = node!.getLeftChild()
				}
				while stack.count > 0{
					node = stack.popLast()!
					if(node.getData() != nil){
						let nodeData = NSMutableAttributedString(string: String(describing: (node.getData())!), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
						let nodeHeight = NSMutableAttributedString(string: (String(describing: (node.getHeight())) + "  "), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 10.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
						attributedTraversal.append(nodeData);
						attributedTraversal.append(nodeHeight);
					}
					if node.getRightChild() != nil{
						node = node.getRightChild()
						while(node != nil){
							stack.append(node)
							node = node.getLeftChild()
						}
					}
				}
			}
		}
		else{
			if t?.getData() != nil{
				var stack: [Node?] = []
				var node: Node! = t
				while node != nil {
					stack.append(node)
					node = node!.getLeftChild()
				}
				while stack.count > 0{
					node = stack.popLast()!
					if(node.getData() != nil){
						let nodeData = NSMutableAttributedString(string: String(describing: (node.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
						attributedTraversal.append(nodeData)
					}
					if node.getRightChild() != nil{
						node = node.getRightChild()
						while(node != nil){
							stack.append(node)
							node = node.getLeftChild()
						}
					}
				}
			}
		}
		return attributedTraversal;
	}
	private func preOrder(top t: Node?) -> NSMutableAttributedString {
		/******************************************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) root, 2) left child, 3) right child
		Node: first checks if tree is red-black; if it is, the nodes can be colored red
		******************************************************************************/
		let traversal: String = "PRE-ORDER\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.gray])
		if(tree_type == "Red-Black Tree"){
			/***********************************************************
			if tree type is red black, this will color the red nodes red
			***********************************************************/
			if t?.getData() != nil {
				var stack: [Node?] = []
				stack.append(t)
				while (stack.count > 0){
					let temp: Node! = stack.popLast()!
					if temp.getData() != nil{
						if temp.getColor() == "R"{
							let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.red]);
							attributedTraversal.append(nodeData);
						}else{
							let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
							attributedTraversal.append(nodeData);
						}
					}
					if temp.getRightChild() != nil{
						stack.append(temp.getRightChild())
					}
					if temp.getLeftChild() != nil{
						stack.append(temp.getLeftChild())
					}
				}
			}
		}
		else if(tree_type == "AVL Tree"){
			/**********************************************************
			if tree type is AVL, it will print the height of every node
			**********************************************************/
			if t?.getData() != nil {
				var stack: [Node?] = []
				stack.append(t)
				while (stack.count > 0){
					let temp: Node! = stack.popLast()!
					let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					let nodeHeight = NSMutableAttributedString(string: (String(describing: (temp.getHeight())) + "  "), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 10.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData);
					attributedTraversal.append(nodeHeight);
					if temp.getRightChild() != nil{
						stack.append(temp.getRightChild())
					}
					if temp.getLeftChild() != nil{
						stack.append(temp.getLeftChild())
					}
				}
			}
		}
		else{
			if t?.getData() != nil {
				var stack: [Node?] = []
				stack.append(t)
				while (stack.count > 0){
					let temp: Node! = stack.popLast()!
					let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData);
					if temp.getRightChild() != nil{
						stack.append(temp.getRightChild())
					}
					if temp.getLeftChild() != nil{
						stack.append(temp.getLeftChild())
					}
				}
			}
		}
		return attributedTraversal
	}
	private func postOrder(top t: Node?) -> NSMutableAttributedString {
		/******************************************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) left child, 2) right child, 3) root
		Node: first checks if tree is red-black; if it is, the nodes can be colored red
		******************************************************************************/
		var stack1: [Node?] = []
		var stack2: [Node?] = []
		let traversal: String = "POST-ORDER\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.gray])
		if tree_type == "Red-Black Tree"{
			/***********************************************************
			if tree type is red black, this will color the red nodes red
			***********************************************************/
			if t?.getData() != nil {
				stack1.append(t)
				while stack1.count > 0 {
					let temp: Node! = stack1.popLast()!
					stack2.append(temp)
					if temp.getLeftChild() != nil {
						stack1.append(temp.getLeftChild())
					}
					if temp.getRightChild() != nil {
						stack1.append(temp.getRightChild())
					}
				}
				while stack2.count > 0 {
					let temp: Node! = stack2.popLast()!
					if temp.getData() != nil {
						if temp.getColor() == "R"{
							let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.red]);
							attributedTraversal.append(nodeData);
						}else{
							let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
							attributedTraversal.append(nodeData);
						}
					}
				}
			}
		}
		else if(tree_type == "AVL Tree"){
			/**********************************************************
			if tree type is AVL, it will print the height of every node
			**********************************************************/
			if t?.getData() != nil {
				stack1.append(t)
				while stack1.count > 0 {
					let temp: Node! = stack1.popLast()!
					stack2.append(temp)
					if temp.getLeftChild() != nil {
						stack1.append(temp.getLeftChild())
					}
					if temp.getRightChild() != nil {
						stack1.append(temp.getRightChild())
					}
				}
				while stack2.count > 0 {
					let temp: Node! = stack2.popLast()!
					let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					let nodeHeight = NSMutableAttributedString(string: (String(describing: (temp.getHeight())) + "  "), attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 10.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData);
					attributedTraversal.append(nodeHeight);
				}
			}
		}
		else{
			if t?.getData() != nil {
				stack1.append(t)
				while stack1.count > 0 {
					let temp: Node! = stack1.popLast()!
					stack2.append(temp)
					if temp.getLeftChild() != nil {
						stack1.append(temp.getLeftChild())
					}
					if temp.getRightChild() != nil {
						stack1.append(temp.getRightChild())
					}
				}
				while stack2.count > 0 {
					let temp: Node! = stack2.popLast()!
					let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData);
				}
			}
		}
		return attributedTraversal
	}
	/************************************************************
	calls the right traversal method based on what the user chose
	************************************************************/
	func traverse(type: String, top t: Node?) -> NSMutableAttributedString? {
		if type == "post-order"{
			return postOrder(top: t)
		}
		else if type == "pre-order"{
			return preOrder(top: t)
		}
		return inOrder(top: t)
	}
	deinit {
		print("TREE KILLED")
	}
}
