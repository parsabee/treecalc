//
//  Heap.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/23/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import Foundation
import UIKit

class Heap{
	private var Max: MaxHeap? = nil;
	private var Min: MinHeap? = nil;
	private var root: Node? = nil;
	var heap_type: String?;
	init(heap: String){
		heap_type = heap;
		switch heap{
		case "Binary Max-Heap":
			Max = MaxHeap();
		case "Binary Min-Heap":
			Min = MinHeap();
		default:
			break;
		}
	}
	/***********
	tree methods
	***********/
	func getTree()->Any?{
		switch heap_type!{
		case "Binary Max-Heap":
			return Max;
		case "Binary Min-Heap":
			return Min;
		default:
			return nil;
		}
	}
	func getRoot() -> Node?{
		switch heap_type! {
		case "Binary Max-Heap":
			return Max!.getRoot();
		case "Binary Min-Heap":
			return Min!.getRoot();
		default:
			break;
		}
		return nil;
	}
	func pop() -> Node?{
		switch heap_type! {
		case "Binary Max-Heap":
			return Max!.popMax();
		case "Binary Min-Heap":
			return Min!.popMin()
		default:
			return nil
		}
	}
	func insert(data: Float32?) -> String?{
		switch heap_type!{
		case "Binary Max-Heap":
			let x = Max!.insert(data: data!);
			if x != 1{
				return "Insertion Failed: Node Already Exists\n"
			}
			break;
		case "Binary Min-Heap":
			let x = Min!.insert(data: data!);
			if x != 1{
				return "Insertion Failed: Node Already Exists\n"
			}
			break;
		default:
			break;
		}
		return nil
	}
	/**************************
	Iterative traversal methods
	**************************/
	private func inOrder(top t: Node?) -> NSMutableAttributedString{
		/******************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) left child, 2) root, 3) right child
		******************************************************/
		let traversal: String = "IN-ORDER\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.gray])
		if t?.getData() != nil{
			var stack: [Node?] = []
			var node: Node! = t
			while node != nil {
				stack.append(node)
				node = node!.getLeftChild()
			}
			while stack.count > 0{
				node = stack.popLast()!
				if(node.getData() != nil){
					let nodeData = NSMutableAttributedString(string: String(describing: (node.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData)
				}
				if node.getRightChild() != nil{
					node = node.getRightChild()
					while(node != nil){
						stack.append(node)
						node = node.getLeftChild()
					}
				}
			}
		}
		return attributedTraversal;
	}
	private func preOrder(top t: Node?) -> NSMutableAttributedString {
		/******************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) root, 2) left child, 3) right child
		******************************************************/
		let traversal: String = "PRE-ORDER\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.white])
		if t?.getData() != nil {
			var stack: [Node?] = []
			stack.append(t)
			while (stack.count > 0){
				let temp: Node! = stack.popLast()!
				let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
				attributedTraversal.append(nodeData);
				if temp.getRightChild() != nil{
					stack.append(temp.getRightChild())
				}
				if temp.getLeftChild() != nil{
					stack.append(temp.getLeftChild())
				}
			}
		}
		return attributedTraversal
	}
	private func postOrder(top t: Node?) -> NSMutableAttributedString {
		/******************************************************
		Iteratively visits every subtrees nodes in this manner:
		1) left child, 2) right child, 3) root
		******************************************************/
		var stack1: [Node?] = []
		var stack2: [Node?] = []
		let traversal: String = "POST-OREDR\n"
		let attributedTraversal = NSMutableAttributedString(string: traversal, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 30.0)!,NSAttributedStringKey.foregroundColor:UIColor.white])
		if t?.getData() != nil {
			if t?.getData() != nil {
				stack1.append(t)
				while stack1.count > 0 {
					let temp: Node! = stack1.popLast()!
					stack2.append(temp)
					if temp.getLeftChild() != nil {
						stack1.append(temp.getLeftChild())
					}
					if temp.getRightChild() != nil {
						stack1.append(temp.getRightChild())
					}
				}
				while stack2.count > 0 {
					let temp: Node! = stack2.popLast()!
					let nodeData = NSMutableAttributedString(string: String(describing: (temp.getData())!) + "  ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 20.0)!,NSAttributedStringKey.foregroundColor:UIColor.white]);
					attributedTraversal.append(nodeData);
				}
			}
		}
		return attributedTraversal
	}
	/************************************************************
	calls the right traversal method based on what the user chose
	************************************************************/
	func traverse(type: String, top t: Node?) -> NSMutableAttributedString? {
		if type == "post-order"{
			return postOrder(top: t)
		}
		else if type == "pre-order"{
			return preOrder(top: t)
		}
		return inOrder(top: t)
	}
	deinit {
		print("HEAP KILLED")
	}
}
