//
//  Node.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/21/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

class Node{
	/*************************************************
	Initiates the nodes that will be added to the tree
	Class global variables
	*************************************************/

	private var data: Float32?;
	/**************************************
	color attribute used for red-black tree
	**************************************/
	private var color: Character;
	/*********************************
	height attribute used for AVL tree
	*********************************/
	private var height: Int;
	private var left_child: Node?;
	private var right_child: Node?;
	weak private var parent: Node?;
	/****************
	Class constructor
	****************/
	init(data: Float32?){
		self.data = data;
		left_child = nil;
		right_child = nil;
		parent = nil;
		color = "B";
		height = 1;
	}
	/************
	Class setters
	************/
	func setData(data: Float32?){
		self.data = data;
	}
	func setLeftChild(left_child: Node?){
		self.left_child = left_child;
	}
	func setRightChild(right_child: Node?){
		self.right_child = right_child;
	}
	func setParent(parent: Node?){
		self.parent = parent;
	}
	func setColor(color: Character){
		self.color = color;
	}
	func setHeight(height: Int){
		self.height = height;
	}
	/************
	Class getters
	************/
	func getData() -> Float32?{
		return data;
	}
	func getLeftChild() -> Node?{
		return left_child;
	}
	func getRightChild() -> Node?{
		return right_child;
	}
	func getParent() -> Node?{
		return parent;
	}
	func getColor() -> Character?{
		return color;
	}
	func getHeight() -> Int{
		return height;
	}
	deinit{
		print("NODE KILLED")
	}
}
