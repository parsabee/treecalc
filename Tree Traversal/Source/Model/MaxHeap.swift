//
//  MaxHeap.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/22/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import Foundation
class MaxHeap{
	private var myHeap: [Node?] = [];
	private var length: Int;
	private var root: Node?;
	init(){
		myHeap.append(nil);
		length = 1;
		root = nil;
	}
	public func getRoot() -> Node?{
		return root;
	}
	private func parent(position: Int) -> Int{
		/******************************************************************************
		takes the position of a node and returns the position of its parent in the heap
		******************************************************************************/
		if(position == 1){
			return position
		}
		return (position/2)
	}
	public func insert(data: Float32) -> Int{
		/*************************
		delete node from the tree,
		returns 1 if successful,
		0 if node already exists
		*************************/
		/************
		No duplicates
		************/
		if search(top: root, data: data) != nil{
			return 0
		}
		var temp: Node? = Node(data: data);
		if (length == 1){
			/********************
			CASE I: tree is empty
			********************/
			myHeap.append(temp);
			length += 1;
			root = temp
		}
		else{
			/*************************
			CASE II: root is not empty
			*************************/
			myHeap.append(temp);
			temp!.setParent(parent: myHeap[parent(position: length)]);
			if (length % 2 == 0){
				/*****************************************************************************
				if the index of the entered node is even, node is the left child of its parent
				*****************************************************************************/
				temp!.getParent()!.setLeftChild(left_child: temp);
			}
			else{
				/*****************************************************************************
				if the index of the entered node is odd, node is the right child of its parent
				*****************************************************************************/
				temp!.getParent()!.setRightChild(right_child: temp);
			}
			length += 1;
		}
		/*************************
		maintain max-heap property
		*************************/
		while(temp!.getParent() != nil && ((temp!.getData())! > (temp!.getParent()!.getData())!)){
			swap(u: temp!, v: (temp!.getParent())!)
			temp = temp!.getParent()
		}
		return 1;
	}
	private func search(top: Node?, data: Float32) -> Node?{
		/****************************************************************************
		private search method; returns the node that corresponds with the input data.
		returns nil if data not found.
		****************************************************************************/
		guard top != nil else{
			return nil;
		}
		if (top!.getData() == data){
			/**********
			found node!
			**********/
			return top;
		}
		else if((top!.getData())! < data){
			/*****************
			node doesn't exist
			*****************/
			return nil
		}
		let rightSearch: Node? = search(top: top?.getRightChild(), data: data);
		let leftSearch: Node? = search(top: top?.getLeftChild(), data: data);
		if(rightSearch != nil){
			return rightSearch;
		}
		else if(leftSearch != nil){
			return leftSearch;
		}
		return nil
	}
	func popMax() -> Node?{
		/**************************************************
		removes and outputs the maximum element of the heap
		**************************************************/
		if (length == 2){
			let temp: Node = myHeap.popLast()!!;
			root = nil
			length -= 1
			return temp
		}
		else if (length > 2){
			let temp: Node = myHeap.popLast()!!;
			swap(u: root!, v: temp)
			if (temp.getParent()?.getRightChild()?.getData() == temp.getData()){
				temp.getParent()?.setRightChild(right_child: nil)
			}
			else if(temp.getParent()?.getLeftChild()?.getData() == temp.getData()){
				temp.getParent()?.setLeftChild(left_child: nil)
			}
			length -= 1
			/************************************
			call to MaxHeapify to restore balance
			************************************/
			MaxHeapify(i: 1);
			return temp;
		}
		return nil
	}
	private func MaxHeapify(i: Int){
		/*******************************************************************
		private function that restores an unbalanced heap back to a max heap
		*******************************************************************/
		let l_sb_index: Int = 2 * i;
		let r_sb_index: Int = (2 * i) + 1;
		var largest: Int = i;
		if (l_sb_index < length && (myHeap[l_sb_index]!.getData())! > (myHeap[largest]!.getData())!){
			largest = l_sb_index;
		}
		if (r_sb_index < length && (myHeap[r_sb_index]!.getData())! > (myHeap[largest]!.getData())!){
			largest = r_sb_index;
		}
		if (largest != i){
			swap(u: myHeap[i]!, v: myHeap[largest]!);
			MaxHeapify(i: largest);
		}
	}
	private func swap(u: Node, v: Node){
		let temp: Float32 = u.getData()!
		u.setData(data: v.getData())
		v.setData(data: temp)
	}
	deinit {
		print("MAX KILLED")
	}
}
