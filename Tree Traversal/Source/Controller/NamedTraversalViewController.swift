//
//  NamedTraversalViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/25/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class NamedTraversalViewController: UIViewController, MFMailComposeViewControllerDelegate, NSFetchedResultsControllerDelegate{
	private var selected_traversal: String = "in-order"
	var name: TreeName!
	private var traversalFetchedResultsController: NSFetchedResultsController<TreeTraversal>!
	@IBOutlet weak var Traversal: UITextView!
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = name.tree_name
		traversalFetchedResultsController = TreeService.shared.TreeTraversals(tree: name)
		traversalFetchedResultsController.delegate = self
		refresh()
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	private func refresh(){
		if (selected_traversal == "post-order"){
			Traversal.attributedText = traversalFetchedResultsController.object(at: IndexPath(row: 0, section:0)).post_order!.rtfToAttributedString
		}
		else if(selected_traversal == "pre-order"){
			Traversal.attributedText = traversalFetchedResultsController.object(at: IndexPath(row: 0, section:0)).pre_order!.rtfToAttributedString
		}
		else{
			Traversal.attributedText = traversalFetchedResultsController.object(at: IndexPath(row: 0, section:0)).tree_traversal!.rtfToAttributedString
		}
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "options" {
			let navController = segue.destination as! UINavigationController
			let optionsController = navController.topViewController as! OptionsViewController
			optionsController.delegate = self
		}
		else {
			super.prepare(for: segue, sender: sender)
		}
	}
}
extension NamedTraversalViewController: OptionsDelegate{
	func selectedTraversal(traversal: String) {
		selected_traversal = traversal
		refresh()
	}
}
/***************************************************
The following sectiong was found on "Stack Overflow"
LINK: https://stackoverflow.com/questions/44390520/cannot-convert-value-of-type-nsattributedstring-documentattributekey-to-documen
***************************************************/
extension String {
	var rtfToAttributedString: NSMutableAttributedString? {
		do {
			return try NSMutableAttributedString(data: Data(utf8),
										  options: [.documentType: NSMutableAttributedString.DocumentType.rtf,
													.characterEncoding: String.Encoding.utf8.rawValue],
										  documentAttributes: nil)
		} catch {
			print("error: ", error)
			return nil
		}
	}
}
