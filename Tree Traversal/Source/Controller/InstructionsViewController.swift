//
//  InstructionsViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/30/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {
	var tree_type: String?
	@IBOutlet weak var instructions: UITextView!
	override func viewDidLoad() {
		super.viewDidLoad()
		/***********************
		setting up swipe motions
		***********************/
		let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		swipeDown.direction = UISwipeGestureRecognizerDirection.down
		self.view.addGestureRecognizer(swipeDown)
		if tree_type == "Binary Search Tree"{
			/******************************
			Binary Search Tree Instructions
			******************************/
			instructions.text = "Binary Search Tree:\n\n    A binary search tree is a data structure in which data is organized in a binary tree. It is represented by a doubly linked data structure. Each node keeps a reference to its parent and its left and right children and if they don't exist, they point to a sentinel node. The keys of the nodes are organized in such way that always satisfy the following property:\n\n    Let x be a node in a binary tree. If y is a node in the left subtree of x, then y.key ≤ x.key. If y is a node in the right subtree of x, then y.key ≥ x.key (However, in this implementation of binary search tree, duplicates are not allowed).\n\n    Basic operations on a binary tree take time proportional to the height of the tree. On average, these operations run on O(lg n), but in the worst case (where the tree is a linear chain of nodes) the run time is Θ(n).\n\nOperations:\n\n(The summery of the inputs will be logged in the console above the number pad.)\n\nInsert: let h be the height of the tree, then the run time is O(h) on average but Θ(n) in the worst case.\nTo insert a node into the tree, type in the number and press the button 'Insert' (No Duplicates).\n\nDelete: let h be the height of the tree, then the run time is O(h) on average but Θ(n) in the worst case.\nTo delete a node from the tree, type in the number and press the button 'Remove' (If the node exists, it'll be removed).\n\nReset: Deletes all the nodes.\n\nTraversal: The run time of each of the three pre-order, in-order, and post-order walks is O(n), where n is the number of nodes.\nTo see the traversal of the tree, press the button 'Traversal.' "
		}else if tree_type == "Red-Black Tree"{
			/**************************
			Red Black Tree Instructions
			**************************/
			instructions.text = "Red-Black Tree:\n\n    A red black tree is a variation of binary search tree. the red black tree is a balanced binary search tree, so the basic operations run in O(lg n) even in the worst case. In addition to the binary search tree property, the red black tree has the following properties:\n\n1. Every node is either red or black.\n2. The root is black.\n3. Every leaf(Nil) is black.\n4. If a node is red, its children are black.\n5. For each node, all simple paths from the node to descendant leaves contain the same number of black nodes.\n\nOperations:\n\n(The summery of the inputs will be logged in the console above the number pad.)\n\nInsert: let h be the height of the tree, then the run time is O(h).\nTo insert a node into the tree, type in the number and press the button 'Insert' (No Duplicates).\n\nDelete: let h be the height of the tree, then the run time is O(h).\nTo delete a node from the tree, type in the number and press the button 'Remove' (If the node exists, it'll be removed).\n\nTraversal: The run time of each of the three pre-order, in-order, and post-order walks is O(n), where n is the number of nodes.\nTo see the traversal of the tree, press the button 'Traversal.' "
		}else if tree_type == "AVL Tree"{
			/********************
			AVL Tree Instructions
			********************/
			instructions.text = "AVL Tree:\n\n    An AVL tree is a variation of binary search tree. AVL tree is a balanced binary search tree, so the basic operations run in O(lg n) even in the worst case. In addition to the binary search tree property, AVL tree has the following height property:\n\n    Let v be an internal node, the height of v's children may differ by at most 1. That is if node v has children x and y, |height.x - height.y| ≤ 1.\n\nOperations:\n\n(The summery of the inputs will be logged in the console above the number pad.)\n\nInsert: let h be the height of the tree, then the run time is O(h).\nTo insert a node into the tree, type in the number and press the button 'Insert' (No Duplicates).\n\nDelete: let h be the height of the tree, then the run time is O(h).\nTo delete a node from the tree, type in the number and press the button 'Remove' (If the node exists, it'll be removed).\n\nTraversal: The run time of each of the three pre-order, in-order, and post-order walks is O(n), where n is the number of nodes.\nTo see the traversal of the tree, press the button 'Traversal.' "
		}else if tree_type == "Binary Min-Heap"{
			/***************************
			Binary Min Heap Instructions
			***************************/
			instructions.text = "Binary Min-Heap:\n\n    The binary heap data structure is an array object that in this implementation is viewed as a nearly complete binary tree. On every level, the nodes are inserted from left to right, resulting in the tree being completely filled on every level except for possibly the lowest level.\n    Let n be a node in the heap with index i in the array. Then the index of its parent in the array is ⌊i/2⌋, the index of its left child is 2i, and the index of its right child is 2i+1.\n\nMin-Heap property:\n\n    Let A be the heap array, for every node other than the root,\nA[parent.i] ≤ A[i],\nthat is, the value of a node is at least the value of its parent, and minimum node is always in the root.\n\nOperations:\n\n(The summery of the inputs will be logged in the console above the number pad.)\n\nInsert: The run time of insertion into a heap is O(lg n). However, in this implementation duplicates are not allowed, which raises the compexity to O(n) in the worst case (Heaps are not very good for implementing a duplicate-free data structure).\nTo insert a node into the heap, type in the number on the number pad and press insert (No Duplicates).\n\nPop-Min: Since the minimum is always in the root, to pop the min is O(1), but to re-heapify the tree is O(h) where h is the height. To pop the minimum, press 'Pop Min', if the heap is not empty, the minimum will be popped and outputted.\n\nReset: Deletes all the nodes in the heap.\n\nTraversal: The run time of each of the three pre-order, in-order, and post-order walks is O(n), where n is the number of nodes.\nTo see the traversal of the tree, press the button 'Traversal.' "
		}else{
			/***************************
			Binary Max Heap Instructions
			***************************/
			instructions.text = "Binary Max-Heap:\n\n    The binary heap data structure is an array object that in this implementation is viewed as a nearly complete binary tree. On every level, the nodes are inserted from left to right, resulting in the tree being completely filled on every level except for possibly the lowest level.\n    Let n be a node in the heap with index i in the array. Then the index of its parent in the array is ⌊i/2⌋, the index of its left child is 2i, and the index of its right child is 2i+1.\n\nMax-Heap property:\n\n    Let A be the heap array, for every node other than the root,\n A[parent.i] ≥ A[i],\nthat is, the value of a node is at most the value of its parent, and maximum node is always in the root.\n\nOperations:\n\n(The summery of the inputs will be logged in the console above the number pad.)\n\nInsert: The run time of insertion into a heap is O(lg n).However, in this implementation duplicates are not allowed, which raises the compexity to O(n) in the worst case (Heaps are not very good for implementing a duplicate-free data structure).\nTo insert a node into the heap, type in the number on the number pad and press insert (No Duplicates).\n\nPop-Max: Since the maximum is always in the root, to pop the max is O(1), but to re-heapify the tree is O(h) where h is the height. To pop the maximum, press 'Pop Max', if the heap is not empty, the maximum will be popped and outputted.\n\nReset: Deletes all the nodes in the heap.\n\nTraversal: The run time of each of the three pre-order, in-order, and post-order walks is O(n), where n is the number of nodes.\nTo see the traversal of the tree, press the button 'Traversal.' "
		}
		instructions.scrollRangeToVisible(NSMakeRange(0, 0))/**make textview show from the top**/
	}
	@IBAction func Done(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	/************
	handle swipes
	************/
	@objc private func swipeAction(swipe: UISwipeGestureRecognizer){
		if swipe.direction == UISwipeGestureRecognizerDirection.down{
			Done(self)
		}
	}
}
