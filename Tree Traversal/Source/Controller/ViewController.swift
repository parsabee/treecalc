//
//  ViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/15/17.
//  Copyright © 2017 PB. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	var tree: String?
	override func viewDidLoad() {
		super.viewDidLoad()
		/***********************
		setting up swipe motions
		***********************/
		let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		upSwipe.direction = UISwipeGestureRecognizerDirection.up
		self.view.addGestureRecognizer(upSwipe)
		let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		swipeDown.direction = UISwipeGestureRecognizerDirection.down
		self.view.addGestureRecognizer(swipeDown)
	}
	@IBAction func aboutDone(_ sender: Any) {
		/************************
		dismisses about modalView
		************************/
		self.dismiss(animated: true, completion: nil)
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	@objc private func swipeAction(swipe: UISwipeGestureRecognizer){
		/*************
		handles swipes
		*************/
		if swipe.direction == UISwipeGestureRecognizerDirection.up{
			performSegue(withIdentifier: "goToAbout", sender: self)
		}
		if swipe.direction == UISwipeGestureRecognizerDirection.down{
			aboutDone(self)
		}
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segue.identifier!{
		case "toBST":
			let treeViewController = segue.destination as! TreeViewController
			treeViewController.tree_type = "Binary Search Tree"
			break;
			/*****************
			TODO: COMPLETE RBT
			*****************/
//		case "toRBT":
//			let treeViewController = segue.destination as! TreeViewController
//			treeViewController.tree_type = "Red-Black Tree"
//			break;
		case "toAVL":
			let treeViewController = segue.destination as! TreeViewController
			treeViewController.tree_type = "AVL Tree"
			break;
		case "toMaxHeap":
			let heapViewController = segue.destination as! HeapViewController
			heapViewController.heap_type = "Binary Max-Heap"
			break;
		case "toMinHeap":
			let heapViewController = segue.destination as! HeapViewController
			heapViewController.heap_type = "Binary Min-Heap"
			break;
		default:
			break;
		}
	}
}

