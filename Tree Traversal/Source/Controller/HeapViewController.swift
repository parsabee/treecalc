//
//  HeapViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 12/23/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//
import UIKit
class HeapViewController: UIViewController {
	var heap_type: String?
	private var heap: Heap?
	var user_input: String = ""
	@IBOutlet weak var poptitle: UIButton!
	@IBOutlet weak var addButton: UIButton!
	@IBOutlet weak var inputSummary: UITextView!
	@IBOutlet weak var resetButton: UIButton!
	@IBOutlet weak var traversalButton: UIButton!
	@IBOutlet weak var numberLabel: UILabel!
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = heap_type!
		heap = Heap(heap: heap_type!)
		if heap_type == "Binary Min-Heap"{
			poptitle.setTitle("Pop Min", for: .normal)
		}
		inputSummary.text = user_input;
		poptitle.titleLabel?.minimumScaleFactor = 0.5
		poptitle.titleLabel?.numberOfLines = 0
		poptitle.titleLabel?.adjustsFontSizeToFitWidth = true
		addButton.titleLabel?.adjustsFontSizeToFitWidth = true
		resetButton.titleLabel?.adjustsFontSizeToFitWidth = true
		traversalButton.titleLabel?.adjustsFontSizeToFitWidth = true
		numberLabel.minimumScaleFactor = 0.5
		numberLabel.adjustsFontSizeToFitWidth = true
		/*****************************************
		dismiss keyboard when tapped on the screen
		*****************************************/
		self.hideKeyboardWhenTappedAroundHeap()
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		/***********************
		setting up swipe motions
		***********************/
		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		leftSwipe.direction = UISwipeGestureRecognizerDirection.left
		self.view.addGestureRecognizer(leftSwipe)
		let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		upSwipe.direction = UISwipeGestureRecognizerDirection.up
		self.view.addGestureRecognizer(upSwipe)
	}
	/*********************************
	setting up IBOutlets and IBActions
	*********************************/
	@IBAction func numberPad(_ sender: UIButton) {
		if (sender.tag == 10){
			if numberLabel.text != "Empty"{
				numberLabel.text! += "."
			}else if numberLabel.text == "-"{
				numberLabel.text! += "0."
			}else{
				numberLabel.text! = "0."
			}
		}else if(sender.tag == 11){
			if numberLabel.text! != "Empty"{
				numberLabel.text!.removeLast()
				if(numberLabel.text! == ""){
					numberLabel.text! = "Empty"
				}
			}
		}else if(sender.tag == 12){
			if numberLabel.text! == "Empty"{
				numberLabel.text! = "-"
			}
		}else{
			if (numberLabel.text! == "Empty" || numberLabel.text! == "0"){
				numberLabel.text! = ""
			}
			numberLabel.text! += String(sender.tag)
		}

	}
	@IBAction func reset(_ sender: Any) {
		/*************
		reset the tree
		*************/
		heap = Heap(heap: heap_type!)
		user_input = ""
		inputSummary.text = user_input
		numberLabel.text! = "Empty"
	}
	@IBAction func pop(_ sender: Any) {
		/****************
		pop from the heap
		****************/
		let node: Node? = heap!.pop()
		if node != nil{
			let OK = UIAlertAction(title: "Okay", style: .default, handler: nil)
			if heap_type == "Binary Max-Heap"{
				user_input += "Popped: \(String(describing: node!.getData()!))\n"
				let Max = UIAlertController(title: "Max Popped", message: "Maximum input was: \(String(describing: node!.getData()!))", preferredStyle: .alert)
				Max.addAction(OK)
				self.present(Max, animated: true, completion: nil)
			}
			else if heap_type == "Binary Min-Heap"{
				user_input += "Popped: \(String(describing: node!.getData()!))\n"
				let Min = UIAlertController(title: "Min Popped", message: "Minimum input was: \(String(describing: node!.getData()!))", preferredStyle: .alert)
				Min.addAction(OK)
				self.present(Min, animated: true, completion: nil)
			}
		}else{
			user_input += "Popping Failed: Heap is Empty\n"
		}
		inputSummary.text = user_input
		inputSummary.scrollToBotom();
	}
	@IBAction func add(_ sender: Any) {
		/**************
		add to the tree
		**************/
		if numberLabel.text! != "Empty"{
			if let userInput = Float32(numberLabel.text!){
				let x = heap!.insert(data: userInput)
				if x != nil{
					user_input += x!
				}else{
					user_input += "Inserted:\(userInput)\n"
				}
				inputSummary.text = user_input;
				inputSummary.scrollToBotom();
			}
		}else{
			user_input += "Insertion Failed: Please Enter a Number\n"
			inputSummary.text = user_input
			inputSummary.scrollToBotom()
		}
		numberLabel.text! = "Empty"
	}
	@IBAction func displayResults(_ sender: Any) {
		/*****************************************
		display the selected traversal of the tree
		*****************************************/
		performSegue(withIdentifier: "displayBST", sender: self)
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "displayHeap"{
			let displayViewController = segue.destination as! DisplayViewController
			displayViewController.heap = heap;
		}else if segue.identifier == "instructions"{
			let instructionsViewController = segue.destination as! InstructionsViewController
			instructionsViewController.tree_type = heap_type!
		}else{
			super.prepare(for: segue, sender: sender)
		}
	}
	@objc private func swipeAction(swipe: UISwipeGestureRecognizer){
		if swipe.direction == UISwipeGestureRecognizerDirection.up{
			performSegue(withIdentifier: "instructions", sender: self)
		}
		if swipe.direction == UISwipeGestureRecognizerDirection.left{
			performSegue(withIdentifier: "displayHeap", sender: self)
		}
	}
	private func alertPusher(){
		/******************************************
		send an alert if user inputs invalid entery
		******************************************/
		let alert = UIAlertController(title: "Invalid Input", message: "Please enter numerals", preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true)
	}
}
extension UIViewController {
	func hideKeyboardWhenTappedAroundHeap() {
		/******************************************************************************
		use this function to dismiss keyboard as the users taps anywhere on the screeen
		******************************************************************************/
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboardHeap))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}
	@objc func dismissKeyboardHeap() {
		view.endEditing(true)
	}
}
/********************************************
 The following sectiong was found on "gitHub"
 LINK: https://gist.github.com/caipivara/75a9a02cb27f02c4b35f
 *******************************************/
extension UITextView {
	func scrollToBotom() {
		let range = NSMakeRange((text as NSString).length - 1, 1);
		scrollRangeToVisible(range);
	}
}
