//
//  TreeViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/16/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import UIKit

class TreeViewController: UIViewController {
	var tree_type: String?
	var user_input: String = ""
	private var tree: Tree?
	/**********************************
	setting up IBOutlets and IBActions
	*********************************/
	@IBOutlet weak var numberLabel: UILabel!
	@IBOutlet weak var inputSummary: UITextView!
	@IBOutlet weak var deleteButton: UIButton!
	@IBOutlet weak var addButton: UIButton!
	@IBOutlet weak var traversalButton: UIButton!
	@IBOutlet weak var resetButton: UIButton!
	override func viewDidLoad() {
		super.viewDidLoad()
		numberLabel.minimumScaleFactor = 0.5
		numberLabel.adjustsFontSizeToFitWidth = true
		deleteButton.titleLabel?.adjustsFontSizeToFitWidth = true
		addButton.titleLabel?.adjustsFontSizeToFitWidth = true
		traversalButton.titleLabel?.adjustsFontSizeToFitWidth = true
		resetButton.titleLabel?.adjustsFontSizeToFitWidth = true
		inputSummary.text = user_input
		self.title = tree_type!
		tree = Tree(tree: tree_type!)
		/**dismiss keyboard when tapped on the screen**/
		self.hideKeyboardWhenTappedAroundTree()
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		/**setting up swipe motions**/
		let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		leftSwipe.direction = UISwipeGestureRecognizerDirection.left
		self.view.addGestureRecognizer(leftSwipe)
		let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
		upSwipe.direction = UISwipeGestureRecognizerDirection.up
		self.view.addGestureRecognizer(upSwipe)
	}
	@IBAction func displayResults(_ sender: Any) {
		/**display the selected traversal of the tree**/
		performSegue(withIdentifier: "displayBST", sender: self)
	}
	@IBAction func numberPad(_ sender: UIButton) {
		if (sender.tag == 10){
			if numberLabel.text != "Empty"{
				numberLabel.text! += "."
			}else if numberLabel.text == "-"{
				numberLabel.text! += "0."
			}else{
				numberLabel.text! = "0."
			}
		}else if(sender.tag == 11){
			if numberLabel.text! != "Empty"{
				numberLabel.text!.removeLast()
				if(numberLabel.text! == ""){
					numberLabel.text! = "Empty"
				}
			}
		}else if(sender.tag == 12){
			if numberLabel.text! == "Empty"{
				numberLabel.text! = "-"
			}
		}else{
			if (numberLabel.text! == "Empty" || numberLabel.text! == "0"){
				numberLabel.text! = ""
			}
			numberLabel.text! += String(sender.tag)
		}
	}
	@IBAction func add(_ sender: Any) {
		if numberLabel.text! != "Empty"{
			if let userInput = Float32(numberLabel.text!){
				let x: String? = tree?.insert(data: userInput)
				if x != nil{
					user_input += x!
				}else{
					user_input += "Inserted: \(userInput)\n"
				}
				inputSummary.text = user_input
				inputSummary.scrollToBotom()
			}
		}else{
			user_input += "Insertion Failed: Please Enter a Number\n"
			inputSummary.text = user_input
			inputSummary.scrollToBotom()
		}
		numberLabel.text! = "Empty"
	}
	@IBAction func remove(_ sender: Any) {
		if numberLabel.text! != "Empty"{
			if let userInput = Float32(numberLabel.text!){
				let x = tree?.delete(data: userInput)
				if x != nil{
					user_input += x!
				}else{
					user_input += "Deleted: \(userInput)\n"
				}
				inputSummary.text = user_input
				inputSummary.scrollToBotom()
			}
		}else{
			user_input += "Deletion Failed: Please Enter a Number\n"
			inputSummary.text = user_input
			inputSummary.scrollToBotom()
		}
		numberLabel.text! = "Empty"
	}
	@IBAction func reset(_ sender: Any) {
		tree = Tree(tree: tree_type!)
		user_input = ""
		inputSummary.text = user_input
		numberLabel.text! = "Empty"
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "displayBST" {
			let displayViewController = segue.destination as! DisplayViewController
			displayViewController.tree = tree
		}else if segue.identifier == "instructions"{
			let instructionsViewController = segue.destination as! InstructionsViewController
			instructionsViewController.tree_type = tree_type!
		}else{
			super.prepare(for: segue, sender: sender)
		}
	}
	@objc private func swipeAction(swipe: UISwipeGestureRecognizer){
		if swipe.direction == UISwipeGestureRecognizerDirection.up{
			performSegue(withIdentifier: "instructions", sender: self)
		}
		if swipe.direction == UISwipeGestureRecognizerDirection.left{
			performSegue(withIdentifier: "displayBST", sender: self)
		}
	}
}
extension UIViewController {
	func hideKeyboardWhenTappedAroundTree() {
		/**use this function to dismiss keyboard as the users taps anywhere on the screeen**/
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboardTree))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}
	@objc func dismissKeyboardTree() {
		view.endEditing(true)
	}
}
