//
//  DisplayViewController.swift
//  Tree Traversal
//
//  Created by Parsa Bagheri on 11/16/17.
//  Copyright © 2017 Parsa Bagheri. All rights reserved.
//

import UIKit
import CoreData

class DisplayViewController: UIViewController {
	/*********************************
	the default traversal is pre-order
	*********************************/
	var traversal: String = "in-order"
	var tree: Tree?
	var heap: Heap?
	private var in_order: NSAttributedString?
	private var pre_order: NSAttributedString?
	private var post_order: NSAttributedString?
	private var nameTextField: UITextField?
	let appDelegate = UIApplication.shared.delegate as! AppDelegate

	@IBOutlet weak var guide: UILabel!
	@IBOutlet weak var traversal_text_field: UITextView!
	override func viewDidLoad() {
		super.viewDidLoad()
		/***********
		CASE I: Tree
		***********/
		if(tree != nil){
			traversal_text_field.attributedText = tree!.traverse(type: traversal, top: tree!.getRoot())
			if (tree?.tree_type! == "Red-Black Tree"){
				let guideString: String = "Red: Red Nodes\n(Other nodes are black)"
                let NSguide = NSMutableAttributedString(string: guideString, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 15.0)!, NSAttributedStringKey.foregroundColor:UIColor.gray])
				NSguide.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSRange(location: 0, length: 3))
				guide.attributedText = NSguide;
			}else if(tree?.tree_type! == "AVL Tree"){
                let guideString: String = "Subscripts represent the height of every node"
                let NSguide = NSMutableAttributedString(string: guideString, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 15.0)!, NSAttributedStringKey.foregroundColor:UIColor.gray])
                guide.attributedText = NSguide;
			}
		}
		/************
		CASE II: Heap
		************/
		else{
			traversal_text_field.attributedText = heap!.traverse(type: traversal, top: heap!.getRoot())
		}
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	private func refresh(){
		if tree != nil{
			traversal_text_field.attributedText = tree!.traverse(type: traversal, top: tree!.getRoot())
		}else{
			traversal_text_field.attributedText = heap!.traverse(type: traversal, top: heap!.getRoot())
		}
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "options" {
			let navController = segue.destination as! UINavigationController
			let optionsController = navController.topViewController as! OptionsViewController
			optionsController.delegate = self
		}
	}
	@IBAction func save(_ sender: UIBarButtonItem) {
		/**********************************************
		alert box to take input for saving to core data
		**********************************************/
		let saveAlertController = UIAlertController(title: "Save Tree",
													message: nil,
													preferredStyle: .alert)
		saveAlertController.addTextField(configurationHandler: nameTextField)
		let saveAction = UIAlertAction(title: "Save", style: .default, handler: self.saveHandler)
		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
		saveAlertController.addAction(saveAction)
		saveAlertController.addAction(cancelAction)
		self.present(saveAlertController, animated: true)
	}
	/********************************************
	functions and textfields used in the AlertBox
	********************************************/
	func nameTextField(textField: UITextField){
		nameTextField = textField
		nameTextField?.placeholder = "Name"
	}
	func saveHandler(alert: UIAlertAction!){
		/********************************
		save handler to save to core data
		********************************/
		if (nameTextField?.text != nil && nameTextField?.text != ""){
			/*****************************************
			convert string to html for saving purposes
			*****************************************/
			let documentAttributes: [NSMutableAttributedString.DocumentAttributeKey: Any] = [.documentType: NSMutableAttributedString.DocumentType.rtf]
			if (tree != nil){
				in_order = tree!.traverse(type: "in-order", top: tree!.getRoot())
				pre_order = tree!.traverse(type: "pre-order", top: tree!.getRoot())
				post_order = tree!.traverse(type: "post-order", top: tree!.getRoot())
			}
			else{
				in_order = heap!.traverse(type: "in-order", top: heap!.getRoot())
				pre_order = heap!.traverse(type: "pre-order", top: heap!.getRoot())
				post_order = heap!.traverse(type: "post-order", top: heap!.getRoot())
			}
			do{
				let in_orderData = try in_order!.data(from: NSMakeRange(0, in_order!.length), documentAttributes: documentAttributes)
				let in_orderHTML = String(data: in_orderData, encoding: .utf8) ?? ""
				let pre_orderData = try pre_order!.data(from: NSMakeRange(0, pre_order!.length), documentAttributes: documentAttributes)
				let pre_orderHTML = String(data: pre_orderData, encoding: .utf8) ?? ""
				let post_orderData = try post_order!.data(from: NSMakeRange(0, post_order!.length), documentAttributes: documentAttributes)
				let post_orderHTML = String(data: post_orderData, encoding: .utf8) ?? ""
				let context = appDelegate.persistentContainer.viewContext
				context.perform {
					let traversal_type = TreeType(context: context)
					let tree_name = TreeName(context: context)
					let tree_traversal = TreeTraversal(context: context)
					traversal_type.tree_type = self.tree?.tree_type ?? self.heap!.heap_type!
					tree_name.tree_name = self.nameTextField!.text
					tree_traversal.tree_traversal = in_orderHTML
					tree_traversal.pre_order = pre_orderHTML
					tree_traversal.post_order = post_orderHTML
					tree_name.name_tree = traversal_type
					tree_name.name_traversal = tree_traversal
					do{
						try context.save()
					}catch{
						fatalError("Failed to save context after inserting data")
					}
				}
			}catch{
				fatalError("Failed to convert to HTMLdata")
			}
		}
		else{
			/************************
			alert if name not entered
			************************/
			let alertController = UIAlertController(title: "Please Enter Name",
													message: nil,
													preferredStyle: .alert)
			let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
			alertController.addAction(okAction)
			self.present(alertController, animated: true)
		}
	}
}
extension DisplayViewController: OptionsDelegate{
	func selectedTraversal(traversal: String) {
		self.traversal = traversal
		refresh()
	}
}
